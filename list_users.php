<?php
	 session_start(); 
	 if(!isset($_SESSION['username'])){
		header("Location: login.php");
	 } 
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoTrader | Dev Test</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="datatable/dataTable.bootstrap.min.css">
	<style>
		.height10{
			height:10px;
		}
		.mtop10{
			margin-top:10px;
		}
		.modal-label{
			position:relative;
			top:7px
		}
	</style>
</head>
<body>
<div class="container">
	<h1 class="page-header text-center">| List users |<h4 style="color:red" class="text-right"><?php echo $_SESSION['username'];?></h4></h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="row">
				<a href="logout.php" class="btn btn-danger pull-right btn-sm"><span class="glyphicon glyphicon-log-out"></span> Logout </a> 
			</div>
			<div class="height10">
			</div>
			<div class="row">
				<table id="myTable" class="table table-bordered table-striped">
					<thead>
						<th>Username</th>
						<th>Type Description</th>
					</thead>
					<tbody>
						<?php
							include_once('connection.php');
							$type_description  = $_SESSION['type_description'];
							$sql = "SELECT us.username, 
							               ut.type_description 
							          FROM users us, user_types ut 
									 WHERE us.type_id  = ut.id
									   AND ut.type_description = '$type_description' ";

							//use for MySQLi-OOP
							$query = $conn->query($sql);
							while($row = $query->fetch_assoc()){
								echo 
								"<tr>
									<td>".$row['username']."</td>
									<td>".$row['type_description']."</td>
								</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="jquery/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="datatable/jquery.dataTables.min.js"></script>
<script src="datatable/dataTable.bootstrap.min.js"></script>
<!-- generate datatable on our table -->
<script>
$(document).ready(function(){
	//inialize datatable
    $('#myTable').DataTable();

    //hide alert
    $(document).on('click', '.close', function(){
    	$('.alert').hide();
    })
});
</script>
</body>
</html>