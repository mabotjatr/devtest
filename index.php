<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>AutoTrader | Dev Test</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="datatable/dataTable.bootstrap.min.css">
	<style>
		.height10{
			height:10px;
		}
		.mtop10{
			margin-top:10px;
		}
		.modal-label{
			position:relative;
			top:7px
		}
	</style>
</head>
<body>
<div class="container">
	<h1 class="page-header text-center">Welcome to AutoTrader Dev Test</h1>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="row">
			<?php
				if(isset($_SESSION['error'])){
					echo
					"
					<div class='alert alert-danger text-center'>
						<button class='close'>&times;</button>
						".$_SESSION['error']."
					</div>
					";
					unset($_SESSION['error']);
				}
				if(isset($_SESSION['warning'])){
					echo
					"
					<div class='alert alert-warning text-center'>
						<button class='close'>&times;</button>
						".$_SESSION['warning']."
					</div>
					";
					unset($_SESSION['warning']);
				}
				if(isset($_SESSION['info'])){
					echo
					"
					<div class='alert alert-info text-center'>
						<button class='close'>&times;</button>
						".$_SESSION['info']."
					</div>
					";
					unset($_SESSION['info']);
				}
				if(isset($_SESSION['success'])){
					echo
					"
					<div class='alert alert-success text-center'>
						<button class='close'>&times;</button>
						".$_SESSION['success']."
					</div>
					";
					unset($_SESSION['success']);
				}
			?>
			</div>
			<div class="row">
				
			</div>
			<div class="height10">
			</div>
			<div class="row">
			<center>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						
						<center><h4 class="modal-title" id="myModalLabel">Login</h4></center>
					</div>
					<div class="modal-body">
					<div class="container-fluid">
					<form method="POST" action="login.php">
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label modal-label">Username:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="username" required>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label modal-label">Password:</label>
							</div>
							<div class="col-sm-10">
								<input type="password" class="form-control" name="password" required>
							</div>
						</div>
					</div> 
					</div>
					<div class="modal-footer">
						<button type="submit" name="login" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Login
			            </br>
					
					</form>
					</div>
					<a href="#adduser" color="red" align="right" data-toggle="modal" >Register</a>

				</div>
            </div>
		  
		    </center>
			</div>
		</div>
	</div>
</div>
<?php include('login_modal.php') ?>
<?php include('add_user_modal.php') ?>
<script src="jquery/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="datatable/jquery.dataTables.min.js"></script>
<script src="datatable/dataTable.bootstrap.min.js"></script>
<!-- generate datatable on our table -->
<script>
$(document).ready(function(){
	//inialize datatable
    $('#myTable').DataTable();

    //hide alert
    $(document).on('click', '.close', function(){
    	$('.alert').hide();
    })
});
</script>
</body>
</html>