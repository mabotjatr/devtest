<?php
	session_start();
	include_once('connection.php');

	if(isset($_POST['add'])){
		$username = $_POST['username'];
		$password = md5($_POST['password']);
		$email = $_POST['email'];
		$status = "0";
		$type_id = $_POST['type_id'];
		$type_desc = $_POST['type_desc'];
		$access_level = $_POST['access_level'];
		/**mail variables**/
		$subject = "Activation";	
		
		//link to update status
		$link = "www.ttstudios.co.za/devtest/edit_status.php?username=".$username."";
		
		$message = 	"<html> 
					  <body> 
						<p style=\"text-align:center;height:100px;background-color:#abc;border:1px solid #456;border-radius:3px;padding:10px;\">
							<b>Activation email</b>
							<br/><br/><br/>
							<a href='$link'>Please Click On This link to activate your account!</a>
						</p>
						<br/><br/>Now you Can send HTML Email
					  </body>
					</html>" ; 
					
		$headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: devtest@autotrader.co.za\r\n"."X-Mailer: php";
		/*check if username name exist*/
		$sql2 = "SELECT us.username 
		           FROM users us, user_types ut 
				  WHERE us.type_id  = ut.id
				    AND us.username = '$username' ";
		/****************************************/			
       $query2 = $conn->query($sql2);
	   if(mysqli_num_rows($query2) > 0){
		   $_SESSION['info'] = 'Username already exists';
		   header('location: index.php');
	   }else{
		    /**inser user data**/
			$sql = "INSERT INTO users (username,password, email,status, type_id) 
			VALUES ('$username','$password','$email', '$status', '$type_id')";
			/**inser user_type data**/
			$sql2 = "INSERT INTO user_types (id,type_description, access_level) 
			VALUES ('$type_id','$type_desc','$access_level')";
			
			if($conn->query($sql) && $conn->query($sql2)){
				if(mail($email,$subject,$message,$headers)){
			
					$_SESSION['success'] = 'User successfully registered';
					header('location: index.php');
				}else{
					$_SESSION['warning'] = 'User successfully registered, but could not send activation email.';
					header('location: index.php');
				}
			}else{
				$_SESSION['error'] = 'Something went wrong while adding user. please check your DB!!!';
				header('location: index.php');
			}
	   }
	}
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}
?>